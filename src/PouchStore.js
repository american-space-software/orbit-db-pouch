'use strict'

const Store = require('orbit-db-store')
const PouchIndex = require('./PouchIndex')

var PouchDB = require('pouchdb')


class PouchStore extends Store {

  constructor(ipfs, id, dbname, options) {

    const db = new PouchDB(`./pouch/${dbname.root}-${dbname.path}`)

    class IndexWrapper extends PouchIndex {
      constructor() {
        super(dbname, db)
      }
    }


    let opts = Object.assign({}, { Index: IndexWrapper })
    Object.assign(opts, options)
    super(ipfs, id, dbname, opts)
    
    this._db = db
    this._type = 'pouch'

  }

  get db() {
    return this._db
  }

  get all () {
    return this._index._index
  }

  async list(offset, limit) {
    return this._index.list(offset, limit)
  }


  async get (key) {
    return this._index.get(key)
  }

  async set (key, data, options = {}) {
    return this.put(key, data, options)
  }

  async put (key, data, options = {}) {
    return this._addOperation({
      op: 'PUT',
      key: key,
      value: data
    }, options)
  }

  async del (key, options = {}) {
    return this._addOperation({
      op: 'DEL',
      key: key,
      value: null
    }, options)
  }

  async load(amount) {
    return super.load(amount)
  }


  /**
   * Drops a database and removes local data
   * @return {[None]}
   */
  async drop () {
    super.drop()

    //Clear pouchdb?
    return this._index.drop()

  }


  async _updateIndex () {
    this._recalculateReplicationMax()
    await this._index.updateIndex(this._oplog)
    this._recalculateReplicationProgress()
  }

  async _updateIndexEntry(entry) {
    this._recalculateReplicationMax()
    await this._index.handleItems([entry])
    this._recalculateReplicationProgress()
  }

  async _addOperation (data, { onProgressCallback, pin = false } = {}) {

    if (this._oplog) {
      // check local cache?
      if (this.options.syncLocal) {
        await this.syncLocal()
      }
      const entry = await this._oplog.append(data, this.options.referenceCount, pin)
      this._recalculateReplicationStatus(this.replicationStatus.progress + 1, entry.clock.time)
      await this._cache.set(this.localHeadsPath, [entry])
      await this._updateIndexEntry(entry)
      
      this.events.emit('write', this.address.toString(), entry, this._oplog.heads)
      if (onProgressCallback) onProgressCallback(entry)
      return entry.hash
    }
  }


}

module.exports = PouchStore
