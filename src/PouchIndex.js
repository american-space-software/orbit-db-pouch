'use strict'

class PouchIndex {

  constructor(dbname, pouch) {

    this._dbname = dbname.path
    this._pouch = pouch

  }

  async get(key) {
    return this._pouch.get(key)
  }

  async put(key, value) {
    value["_id"] = key
    return this._pouch.put(value)
  }

  async remove(key) {

    let existing = await this.get(key)

    if (existing) {

      //Mark as deleted then put
      existing._deleted = true
      return this.put(key, existing)

    }
    
  }

  async updateIndex(oplog) {

    let toHandle = []

    let values = oplog.values
      .slice()

    //Figure out which have been handled 
    for (let value of values) {

      //If it's not been handled mark it
      let result = await this._pouch.get(`_local/${value.hash}`)
      let handled = result?.exists == true

      if (!handled) {
        toHandle.push(value)
      }

    }

    await this.handleItems(toHandle)

  }

  async handleItems(toHandle) {
 
    if (!toHandle || toHandle.length == 0) return

    for (let item of toHandle) {

      if (item.payload.op === 'PUT') {
        await this.put(item.payload.key, item.payload.value)
      }
      else if (item.payload.op === 'DEL') {
        await this.remove(item.payload.key)
      }

      //Mark as handled
      await this._pouch.put({
        _id: `_local/${item.hash}`,
        exists: true
      })

    }

  }

  async drop() {
    this._pouch.destroy()
  }



}

module.exports = PouchIndex
