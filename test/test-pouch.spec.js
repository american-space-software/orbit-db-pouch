// @ts-nocheck
const PouchStore = require('../src/PouchStore')

var assert = require('assert')

const OrbitDB = require('orbit-db')
const IPFS = require('ipfs')


var PouchDB = require('pouchdb')
var PouchFind = require('pouchdb-find')

//Enable find plugin
PouchDB.plugin(PouchFind)

describe('PouchStore', async () => {

    let store
    let ipfs
    let orbitdb

    before('Setup', async () => {

        OrbitDB.addDatabaseType("pouch", PouchStore)

        ipfs = await IPFS.create({})

        orbitdb = await OrbitDB.createInstance(ipfs)
        
        store = await orbitdb.open("testtable", {
            create: true,
            type: "pouch"
        })
        

        await store.db.createIndex( { index: { fields: ['currentTeam'] }})
        await store.db.createIndex( { index: { fields: ['throwingHand'] }})
        await store.db.createIndex( { index: { fields: ['battingHand'] }})


        await store.load()
    })

    after('Teardown', async () => {
        await store.drop()
    })





    it('should put items in the store and retreive them by key', async () => {

        //Arrange 
        await store.put("1", {
            name: "Pat"
        })

        await store.put("2", {
            name: "Bill"
        })

        await store.put("3", {
            name: "Jim"
        })

        await store.put("4", {
            name: "Susan"
        })


        //Act
        let pat = await store.get("1")
        let bill = await store.get("2")
        let jim = await store.get("3")
        let susan = await store.get("4")


        //Assert
        assert.equal(pat.name, "Pat")
        assert.equal(bill.name, "Bill")
        assert.equal(jim.name, "Jim")
        assert.equal(susan.name, "Susan")

    })


    it('should put many items and read', async () => {

        console.time('Putting 25 records in mfsstore')
        for (let i = 5; i < 30; i++) {
            await store.put(i.toString(), {
                name: `Pat${i}`
            })
        }
        console.timeEnd('Putting 25 records in mfsstore')

        //Act
        console.time('Reading 25 records mfsstore')
        for (let i = 5; i < 30; i++) {
            let value = await store.get(i.toString())
            assert.equal(value.name, `Pat${i}`)
        }
        console.timeEnd('Reading 25 records mfsstore')


    })





    it('should retreive values by secondary indexes', async () => {

        //Arrange 
        await store.put("101", {
            name: "Andrew McCutchen",
            currentTeam: "PIT",
            battingHand: "R",
            throwingHand: "R"
        })

        await store.put("102", {
            name: "Pedro Alvarez",
            currentTeam: "BAL",
            battingHand: "R",
            throwingHand: "R"
        })

        await store.put("103", {
            name: "Jordy Mercer",
            currentTeam: "PIT",
            battingHand: "L",
            throwingHand: "R"
        })


        await store.put("104", {
            name: "Doug Drabek",
            currentTeam: "BAL",
            battingHand: "L",
            throwingHand: "R"
        })


        //Act
        let teamPIT = await store.db.find({
            selector: {currentTeam: 'PIT'},
            fields: ['_id', 'name', 'currentTeam'],
            sort: ['currentTeam']
        })

        let teamBAL = await store.db.find({
            selector: {currentTeam: 'BAL'},
            fields: ['_id', 'name', 'currentTeam'],
            sort: ['currentTeam']
        })


        let battingR = await store.db.find({
            selector: {battingHand: 'R'},
            fields: ['_id', 'name', 'battingHand'],
            sort: ['battingHand']
        })

        let battingL = await store.db.find({
            selector: {battingHand: 'L'},
            fields: ['_id', 'name', 'battingHand'],
            sort: ['battingHand']
        })


        let throwingR = await store.db.find({
            selector: {throwingHand: 'R'},
            fields: ['_id', 'name', 'throwingHand'],
            sort: ['throwingHand']
        })


        //Teams
        assert.equal(teamPIT.docs[0].name, "Andrew McCutchen")
        assert.equal(teamPIT.docs[1].name, "Jordy Mercer")

        assert.equal(teamBAL.docs[0].name, "Pedro Alvarez")
        assert.equal(teamBAL.docs[1].name, "Doug Drabek")

        //Batting
        assert.equal(battingR.docs[0].name, "Andrew McCutchen")
        assert.equal(battingR.docs[1].name, "Pedro Alvarez")

        assert.equal(battingL.docs[0].name, "Jordy Mercer")
        assert.equal(battingL.docs[1].name, "Doug Drabek")

        //Pitching
        assert.equal(throwingR.docs[0].name, "Andrew McCutchen")
        assert.equal(throwingR.docs[1].name, "Pedro Alvarez")
        assert.equal(throwingR.docs[2].name, "Jordy Mercer")
        assert.equal(throwingR.docs[3].name, "Doug Drabek")


    })


    it('should update a row from one secondary index to another', async () => {

        //Act
        let existing = await store.get("101")



        await store.put("101", {
            name: "Andrew McCutchen",
            currentTeam: "PIT",
            battingHand: "L", //was R
            throwingHand: "R",
            _rev: existing._rev
        })


        //Assert
        let battingR = await store.db.find({
            selector: {battingHand: 'R'},
            fields: ['_id', 'name', 'battingHand'],
            sort: ['battingHand']
        })

        let battingL = await store.db.find({
            selector: {battingHand: 'L'},
            fields: ['_id', 'name', 'battingHand'],
            sort: ['battingHand']
        })

        assert.equal(battingR.docs.length, 1)
        assert.equal(battingR.docs[0]._id != "101", true)

        assert.equal(battingL.docs[0]._id, "101")

    })


    it('should close and reload', async () => {

        //Reload
        console.time('Reload')
        await store.close()

        let opened = await orbitdb.open(store.address)

        await opened.load()
        console.timeEnd('Reload')

        let all = await opened.db.allDocs()

        let battingL = await store.db.find({
            selector: {battingHand: 'L'},
            fields: ['_id', 'name', 'battingHand'],
            sort: ['battingHand']
        })

        assert.equal(all.rows.length, 36)

        assert.equal(battingL.docs[0].name, "Andrew McCutchen")
        assert.equal(battingL.docs[1].name, "Jordy Mercer")

    })

    it('should drop a store', async () => {

        await store.drop()
        await store.close()

        let opened = await orbitdb.open(store.address)

        await opened.load()
        let all = await opened.db.allDocs()

        assert.equal(all.rows.length, 0)

        //Check handled and index maps


    })



})