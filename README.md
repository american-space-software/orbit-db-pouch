# orbit-db-pouch

[![Gitter](https://img.shields.io/gitter/room/nwjs/nw.js.svg)](https://gitter.im/orbitdb/Lobby) [![Matrix](https://img.shields.io/badge/matrix-%23orbitdb%3Apermaweb.io-blue.svg)](https://riot.permaweb.io/#/room/#orbitdb:permaweb.io) [![Discord](https://img.shields.io/discord/475789330380488707?color=blueviolet&label=discord)](https://discord.gg/cscuf5T)

> Key-Value database for [orbit-db](https://github.com/haadcode/orbit-db) backed by [PouchDB](https://pouchdb.com/).

A key-value datastore for OrbitDB backed by the PouchDB. Orbit handles IPFS + authentication + creating a cryptographic trail of changes. PouchDB is really good at being a searchable database with strong revisioning.

Used in [orbit-db](https://github.com/orbitdb/orbit-db).


## Install


## Usage

First, create an instance of OrbitDB:

```javascript
const PouchStore = require('orbit-db-pouch')
const IPFS = require('ipfs')
const OrbitDB = require('orbit-db')

const ipfs = new IPFS()
const orbitdb = await OrbitDB.createInstance(ipfs)
```

Add custom datastore type

```javascript
OrbitDB.addDatabaseType("pouch", PouchStore)
```

Create a datastore with a schema. In this example we're saving baseball players. We'll add 4 different indexed fields. Indexes and can unique. 

```javascript
store = await orbitdb.open("baseballplayers", {
    create: true, 
    type: "pouch"
})
```
Once opened the PouchDB can be accessed with ".db":
```javascript
await store.db.allDocs() //see PouchDB documentation for more.
```


## License
[MIT](LICENSE)
